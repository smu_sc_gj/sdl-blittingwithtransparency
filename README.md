# DESCRIPTION #
This code sample:
- Opens a window 
- Loads a bitmap (sprite sheet)
- Creates an optimised version of the origianl bitmap image
- Sets up a transparency colour (one to be ignored when blitting)
- Blit a region of this original image to a region of the screen. 

# INSTRUCTIONS #
Not written yet. 

# CREDITS #
Loosely based on tutorials from [Lazy Foo](http://lazyfoo.net/SDL_tutorials/) used with permission. 